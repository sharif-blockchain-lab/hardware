// Server CONFIG
var fs = require('fs');
var privateKey  = fs.readFileSync('key/SCM_Server_rsa', 'utf8');
var certificate = fs.readFileSync('key/SCM_Server_rsa.crt', 'utf8');

var credentials = {key: privateKey, cert: certificate};

var host = 'localhost' ;
var httpPort = 9080 ;
var httpsPort = 9443 ;

module.exports.credentials = credentials ;
module.exports.host = host ;
module.exports.httpPort = httpPort ;
module.exports.httpsPort = httpsPort ;
