const Pool = require('pg').Pool







async function addUser(username, password, email, name, familyname) {
  const pool = new Pool({
    user: 'postgres',
    host: 'localhost',
    database: 'test',
    password: '18811376',
    port: 5432,
  })
  var response ;
  const add_user = 'INSERT INTO user_list(username,password,email,name,familyname) VALUES($1,$2,$3,$4,$5) RETURNING *';
  var user = [username, password, email, name, familyname]
  pool.query(add_user, user, (error, results) => {
    if (error) {
      var message ;
      if (error.code == 23505){
        if (error.constraint == 'user_list_username_key' ){
          message = 'username exist'
        }
        if (error.constraint == 'user_list_email_key' ){
          message = 'email exist'
        }

      }
      else{
        message = "db error"
      }

      response = {
       "success": false,
       "message": message,
     }
      console.log('----------------------------------------');
      console.log(JSON.stringify(error));
    }
    else{
      response = {
       "success": true,
       "message": "db successful ",
     }
     console.log('add');
     console.log(JSON.stringify(results));
    }

  });

  await pool.end() ;
  console.log('pool connection closed ');

  return response ;
}





async function findUserByUsername(username) {
  const pool = new Pool({
    user: 'postgres',
    host: 'localhost',
    database: 'test',
    password: '18811376',
    port: 5432,
  })

  const user_find = 'SELECT * FROM user_list WHERE username = $1';
  var user = [username]  ;
  var response = '' ;
  pool.query(user_find, user, (error, results) => {
    if (error) {
      response = {
       "success": false,
       "message": 'findUser database Error',
     }
      console.log('--------------------find user Error --------------------');
      console.log(JSON.stringify(error));
    }
    else{
      if (results.rowCount > 0 ){
        user = results.rows[0]
        response = {
         "success": true,
         "message": "db successful ",
         "user" : user
       }
      }
      else {
       response = {
        "success": false,
        "message": "user not found",
      }
    }

     console.log('---------------- find User Response -----------------');
     console.log(JSON.stringify(results));
    }

  });

  await pool.end() ;
  console.log('pool connection closed ');
  return response ;
}

async function findAllUser() {
  const pool = new Pool({
    user: 'postgres',
    host: 'localhost',
    database: 'test',
    password: '18811376',
    port: 5432,
  })

  const user_find = 'SELECT username FROM user_list';
  var response = '' ;
  pool.query(user_find, (error, results) => {
    if (error) {
      response = {
       "success": false,
       "message": 'findUser database Error',
     }
      console.log('--------------------find user Error --------------------');
      console.log(JSON.stringify(error));
    }
    else{
      if (results.rowCount > 0 ){
        users = results.rows
        response = {
         "success": true,
         "message": "get users Successfully ",
         "usersCount" : results.rowCount,
         "users" : users
       }
      }
      else {
       response = {
        "success": false,
        "message": "no user found",
      }
    }

     console.log('---------------- find User Response -----------------');
     console.log(JSON.stringify(results));
    }

  });

  await pool.end() ;
  console.log('pool connection closed ');
  return response ;
}


async function findConfirmedUser() {
  const pool = new Pool({
    user: 'postgres',
    host: 'localhost',
    database: 'test',
    password: '18811376',
    port: 5432,
  })

  const user_find = 'SELECT username FROM user_list WHERE isConfirmed = true';
  var response = '' ;
  pool.query(user_find, (error, results) => {
    if (error) {
      response = {
       "success": false,
       "message": 'findUser database Error',
     }
      console.log('--------------------find user Error --------------------');
      console.log(JSON.stringify(error));
    }
    else{
      if (results.rowCount > 0 ){
        users = results.rows
        response = {
          "success": true,
          "message": "get users Successfully ",
          "usersCount" : results.rowCount,
          "users" : users
       }
      }
      else {
       response = {
        "success": false,
        "message": "user not found",
      }
    }

     console.log('---------------- find User Response -----------------');
     console.log(JSON.stringify(results));
    }

  });

  await pool.end() ;
  console.log('pool connection closed ');
  return response ;
}


async function findRejectedUser() {
  const pool = new Pool({
    user: 'postgres',
    host: 'localhost',
    database: 'test',
    password: '18811376',
    port: 5432,
  })

  const user_find = 'SELECT username FROM user_list WHERE isConfirmed = false';
  var response = '' ;
  pool.query(user_find, (error, results) => {
    if (error) {
      response = {
       "success": false,
       "message": 'findUser database Error',
     }
      console.log('--------------------find user Error --------------------');
      console.log(JSON.stringify(error));
    }
    else{
      if (results.rowCount > 0 ){
        users = results.rows
        response = {
          "success": true,
          "message": "get users Successfully ",
          "usersCount" : results.rowCount,
          "users" : users
       }
      }
      else {
       response = {
        "success": false,
        "message": "user not found",
      }
    }

     console.log('---------------- find User Response -----------------');
     console.log(JSON.stringify(results));
    }

  });

  await pool.end() ;
  console.log('pool connection closed ');
  return response ;
}


async function findNotseenUser() {
  const pool = new Pool({
    user: 'postgres',
    host: 'localhost',
    database: 'test',
    password: '18811376',
    port: 5432,
  })

  const user_find = 'SELECT username FROM user_list WHERE isConfirmed IS NULL';
  var response = '' ;
  pool.query(user_find, (error, results) => {
    if (error) {
      response = {
       "success": false,
       "message": 'findUser database Error',
     }
      console.log('--------------------find user Error --------------------');
      console.log(JSON.stringify(error));
    }
    else{
      if (results.rowCount > 0 ){
        users = results.rows
        response = {
          "success": true,
          "message": "get users Successfully ",
          "usersCount" : results.rowCount,
          "users" : users
       }
      }
      else {
       response = {
        "success": false,
        "message": "user not found",
      }
    }

     console.log('---------------- find User Response -----------------');
     console.log(JSON.stringify(results));
    }

  });

  await pool.end() ;
  console.log('pool connection closed ');
  return response ;
}


async function adminConfirmUser(username,accountNumber) {
  const pool = new Pool({
    user: 'postgres',
    host: 'localhost',
    database: 'test',
    password: '18811376',
    port: 5432,
  })

  const user_confirm = 'UPDATE user_list SET account_number = $2,isConfirmed = true  WHERE username = $1;';
  var user =[username , accountNumber ]
  var response = '' ;
  pool.query(user_confirm,user , (error, results) => {
    if (error) {
      if (error.code == 23505){
        response = {
         "success": false,
         "message": 'accountnumber exsit',
       }
      }
      else {
        response = {
         "success": false,
         "message": 'confirm database Error',
       }
      }
      console.log('--------------------confirm user Error --------------------');
      console.log(JSON.stringify(error));
    }
    else{
        response = {
          "success": true,
          "message": "confirm users Successfully ",
        }

     console.log('---------------- confirm User Response -----------------');
     console.log(JSON.stringify(results));
    }

  });

  await pool.end() ;
  console.log('pool connection closed ');
  return response ;
}

async function adminRejectUser(username) {
  const pool = new Pool({
    user: 'postgres',
    host: 'localhost',
    database: 'test',
    password: '18811376',
    port: 5432,
  })

  const user_reject = 'UPDATE user_list SET isConfirmed = false  WHERE username = $1';
  var user =[username]
  var response = '' ;
  pool.query(user_reject,user , (error, results) => {
    if (error) {
      response = {
       "success": false,
       "message": 'reject database Error',
     }
      console.log('--------------------find user Error --------------------');
      console.log(JSON.stringify(error));
    }
    else{
        response = {
          "success": true,
          "message": "reject users Successfully ",
       }

     console.log('---------------- find User Response -----------------');
     console.log(JSON.stringify(results));
    }

  });

  await pool.end() ;
  console.log('pool connection closed ');
  return response ;
}


// // user_id serial PRIMARY KEY,
// // username VARCHAR (50) UNIQUE NOT NULL,
// // password VARCHAR NOT NULL,
// // email VARCHAR (355) UNIQUE NOT NULL,
// // name VARCHAR (50) NOT NULL ,
// // familyname VARCHAR (50) NOT NULL ,
// // isConfirmed BOOLEAN ,
// // created_on TIMESTAMP NOT NULL,
// // last_login TIMESTAMP0.
// // username,password,email,name,familyname,isConfirmed,created_on,last_login

 exports.addUser = addUser;
 exports.findUserByUsername = findUserByUsername;
 exports.findAllUser = findAllUser;
 exports.findConfirmedUser = findConfirmedUser;
 exports.findRejectedUser = findRejectedUser;
 exports.findNotseenUser = findNotseenUser;
 exports.adminConfirmUser = adminConfirmUser;
 exports.adminRejectUser = adminRejectUser;
