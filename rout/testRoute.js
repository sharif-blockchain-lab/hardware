//main router
var express = require('express')
var router = express.Router() ;
var mydb = require('../database/database_func.js') ;

// middleware that is specific to this router



router.use(function(req,res, next) {
  console.log('------------------------------ insid test -----------------------' );
  return next();
}) ;


// main route
router.get('/', function (req, res) {
  res.send('run other test')
})
// define the about route
router.post('/test1', function (req, res) {
  res.send('test1 run')
})


router.post('/finduser', async function(req, res) {
  var response = '' ;
  var username = req.body.username;
  var dbresponse = await mydb.findUserByUsername(username)
  console.log(JSON.stringify(dbresponse)) ;
  if (dbresponse.success){
    response = {
      success : true ,
      message : "user find Successfully" ,
      user : dbresponse.user
    }
  }
  else{
    response = {
      "success": false,
      "message": dbresponse.message,
    }
    res.status(401)
  }

  res.json(response)
});

router.post('/findalluser', async function(req, res) {
  var response = '' ;
  var username = req.body.username;
  var dbresponse = await mydb.findAllUser()
  console.log(JSON.stringify(dbresponse)) ;
  if (dbresponse.success){
    response = {
      success : true ,
      message : "user find Successfully" ,
      user : dbresponse.user
    }
  }
  else{
    response = {
      "success": false,
      "message": dbresponse.message,
    }
    res.status(401)
  }

  res.json(dbresponse)
});

router.post('/findconfirmeduser', async function(req, res) {
  var response = '' ;
  var username = req.body.username;
  var dbresponse = await mydb.findConfirmedUser()
  console.log(JSON.stringify(dbresponse)) ;
  if (dbresponse.success){
    response = {
      success : true ,
      message : "user find Successfully" ,
      user : dbresponse.user
    }
  }
  else{
    response = {
      "success": false,
      "message": dbresponse.message,
    }
    res.status(401)
  }

  res.json(dbresponse)
});

router.post('/findRejectedUser', async function(req, res) {
  var response = '' ;
  var username = req.body.username;
  var dbresponse = await mydb.findRejectedUser()
  console.log(JSON.stringify(dbresponse)) ;
  if (dbresponse.success){
    response = {
      success : true ,
      message : "user find Successfully" ,
      user : dbresponse.user
    }
  }
  else{
    response = {
      "success": false,
      "message": dbresponse.message,
    }
    res.status(401)
  }

  res.json(dbresponse)
});

router.post('/findNotseenuser', async function(req, res) {
  var response = '' ;
  var username = req.body.username;
  var dbresponse = await mydb.findNotseenUser()
  console.log(JSON.stringify(dbresponse)) ;
  if (dbresponse.success){
    response = {
      success : true ,
      message : "user find Successfully" ,
      user : dbresponse.user
    }
  }
  else{
    response = {
      "success": false,
      "message": dbresponse.message,
    }
    res.status(401)
  }

  res.json(dbresponse)
});

router.post('/adminConfirmUser', async function(req, res) {
  var response = '' ;
  var username = req.body.username;
  var accountNumber = req.body.accountNumber;
  var isConfirmed = req.body.isConfirmed ;
  console.log(isConfirmed);
  if (isConfirmed == 'true'){
    var dbresponse = await mydb.adminConfirmUser(username,accountNumber)
  }
  else{
    var dbresponse = await mydb.adminRejectUser(username)
  }

  console.log(JSON.stringify(dbresponse)) ;
  if (dbresponse.success){
    response = {
      success : true ,
      message : "user find Successfully" ,
      user : dbresponse.user
    }
  }
  else{
    response = {
      "success": false,
      "message": dbresponse.message,
    }
    res.status(401)
  }

  res.json(dbresponse)
});














module.exports = router
