const Sequelize = require('sequelize');

// Option 1: Passing parameters separately
const sequelize = new Sequelize('test', 'postgres', '18811376', {
  host: 'localhost',
  dialect: 'postgres'
});



sequelize.authenticate().then(() => {
    console.log('Connection has been established successfully.');
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err);
  });


  const User = sequelize.define('user', {
    firstName: {
      type: Sequelize.STRING,
      allowNull: false
    },
    lastName: {
      type: Sequelize.STRING
      // allowNull defaults to true
    }
  }, {
    // options
  });
