//In the name of god

var http = require('http');
var https = require('https');

var express = require('express');
var bodyParser = require('body-parser');
var cors = require('cors');
var jwt = require('jsonwebtoken');


//--------------------config ---------------------------------
var config = require('./Config.js');
//----------------- database------------------------
var mydb = require('./database/database_func.js')
//---------------- route ----------------------------
var protectedRoute = require('./rout/protectedRoute.js')
var testRoute = require('./rout/testRoute.js')
//----------------------------------------------------------------

var app = express();
app.options('*', cors());
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false
}));

app.set('secret', 'RoLaKo4_kNl10LW6nHG2_Gbr5POAuqd9mMVAcbAgbLYvIJTNG2A6nm15ZxrYfEt3bK_fZhsxhZ5mXX9Q-ZsXSQ');




//----------------------------------------------------------------------

//var httpServer = http.createServer(app);
var httpsServer = https.createServer(config.credentials, app);

//httpServer.listen(config.httpPort);
httpsServer.listen(config.httpsPort);

//httpServer.timeout = 240000;
httpsServer.timeout = 240000;

//--------------------------------Start---------------------------------

console.log('****************** SERVER STARTED ************************');
//console.log('***************  http://%s:%s  ******************', config.host, config.httpPort);
console.log('***************  https://%s:%s  ******************', config.host, config.httpsPort);



//--------global function------------------

app.use(function(req, res, next) {
  console.log('<<<<<<<<<<<<<<<<< R E C E I V E D >>>>>>>>>>>>>>>>>');
  console.log('api =' + req.originalUrl)
  console.log('Time: ', Date(Date.now()) ) ;
  next();
})



//-------------- Server route ------------------------------
app.use('/safe', protectedRoute);
app.use('/test', testRoute);





//----------------------- default rout  -------------------------

app.get('/', (req, res) => {
  res.send('OK')
})



app.post('/register', async function(req, res) {
  var response ;
  var username = req.body.username;
  var password = req.body.password;
  var email = req.body.email;
  var name = req.body.name;
  var familyname = req.body.familyname;

  var dbresponse = await mydb.addUser(username, password, email, name, familyname)
  console.log(JSON.stringify(dbresponse)) ;
  if (dbresponse.success){
    response = {
      "success": true,
      "message": "register successful ",
    }
  }
  else{
    response = {
      "success": false,
      "message": dbresponse.message,
    }
    res.status(406)
  }

  res.json(response)
});



app.post('/test', async function(req, res) {

  var response = {
    "success": true,
    "message": "test successful",
  }
  console.log(JSON.stringify(response))
  res.json(response)
});

app.post('/login', async function(req, res) {
  var username = req.body.username;
  var pass = req.body.pass;
  var user = '';
  user = users.find(user => user.name == username && user.pass == pass)
  var response = '';
  if (user) {
    var token = jwt.sign({
      exp: Math.floor(Date.now() / 1000) + 100,
      username: username,
      orgName: 'Micro',
    }, app.get('secret'));

    var response = {
      "success": true,
      "message": "user login Successfully",
      "username": user.name,
      "token": token
    }
    res.json(response)
    console.log("ok" + response);
  } else {
    console.log({
      username,
      pass
    })
    var response = {
      "success": 'green',
      "message": "error login ",
      "username": username,
    }
    res.json(response)
  }

});
